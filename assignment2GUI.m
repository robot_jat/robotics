function varargout = assignment2GUI(varargin)
% ASSIGNMENT2GUI MATLAB code for assignment2GUI.fig
%      ASSIGNMENT2GUI, by itself, creates a new ASSIGNMENT2GUI or raises the existing
%      singleton*.
%
%      H = ASSIGNMENT2GUI returns the handle to a new ASSIGNMENT2GUI or the handle to
%      the existing singleton*.
%
%      ASSIGNMENT2GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in ASSIGNMENT2GUI.M with the given input arguments.
%
%      ASSIGNMENT2GUI('Property','Value',...) creates a new ASSIGNMENT2GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before assignment2GUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to assignment2GUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help assignment2GUI

% Last Modified by GUIDE v2.5 18-Oct-2016 14:58:44

% Begin initialization code - DO NOT EDIT



gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @assignment2GUI_OpeningFcn, ...
                   'gui_OutputFcn',  @assignment2GUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before assignment2GUI is made visible.
function assignment2GUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to assignment2GUI (see VARARGIN)

% Choose default command line output for assignment2GUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes assignment2GUI wait for user response (see UIRESUME)
% uiwait(handles.a2gui);




% --- Outputs from this function are returned to the command line.
function varargout = assignment2GUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on slider movement.
function joint1Slider_Callback(hObject, eventdata, handles)
% hObject    handle to joint1Slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
q1 = degtorad(get(hObject,'Value'));
q2handle = findobj('Tag', 'joint2Slider');
q2 = degtorad(get(q2handle, 'Value'));
q3handle = findobj('Tag', 'joint3Slider');
q3 = degtorad(get(q3handle, 'Value'));
q5handle = findobj('Tag', 'joint4Slider');
q5 = degtorad(get(q5handle, 'Value'));
q4 = 2*pi - q3 - q2;
q = [q1, q2, q3, q4, q5];
doBotTeach(q,1);

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function joint1Slider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to joint1Slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function joint2Slider_Callback(hObject, eventdata, handles)
% hObject    handle to joint2Slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
q2 = degtorad(get(hObject,'Value'));
q1handle = findobj('Tag', 'joint1Slider');
q1 = degtorad(get(q1handle, 'Value'));
q3handle = findobj('Tag', 'joint3Slider');
q3 = degtorad(get(q3handle, 'Value'));
q5handle = findobj('Tag', 'joint4Slider');
q5 = degtorad(get(q5handle, 'Value'));
q4 = 2*pi - q3 - q2;
q = [q1, q2, q3, q4, q5];
doBotTeach(q,1);

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function joint2Slider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to joint2Slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function joint3Slider_Callback(hObject, eventdata, handles)
% hObject    handle to joint3Slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
q3 = degtorad(get(hObject,'Value'));
q2handle = findobj('Tag', 'joint2Slider');
q2 = degtorad(get(q2handle, 'Value'));
q1handle = findobj('Tag', 'joint1Slider');
q1 = degtorad(get(q1handle, 'Value'));
q5handle = findobj('Tag', 'joint4Slider');
q5 = degtorad(get(q5handle, 'Value'));
q4 = 2*pi - q3 - q2;
q = [q1, q2, q3, q4, q5];
doBotTeach(q,1);

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function joint3Slider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to joint3Slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function joint4Slider_Callback(hObject, eventdata, handles)
% hObject    handle to joint4Slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
q5 = degtorad(get(hObject,'Value'));
q2handle = findobj('Tag', 'joint2Slider');
q2 = degtorad(get(q2handle, 'Value'));
q3handle = findobj('Tag', 'joint3Slider');
q3 = degtorad(get(q3handle, 'Value'));
q1handle = findobj('Tag', 'joint1Slider');
q1 = degtorad(get(q1handle, 'Value'));
q4 = 2*pi - q3 - q2;
q = [q1, q2, q3, q4, q5];
doBotTeach(q,1);

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function joint4Slider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to joint4Slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close all
clc


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in demo.
function demo_Callback(hObject, eventdata, handles)
% hObject    handle to demo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
assignment2New();


% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
q = [0 deg2rad(45) deg2rad(0) pi/4 0];
doBotTeach(q,1);
