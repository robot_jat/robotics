function [] = Assignment2()
clear all
clc
%set(0, 'DefaultFigureWindowStyle','docked')

q = [0 pi/4 -pi/2 pi/4 0];
q1Lim = [deg2rad(-135), deg2rad(135)];
q2Lim = [deg2rad(5), deg2rad(80)];
q3Lim = [deg2rad(-170), deg2rad(5)];
q5Lim = [deg2rad(-90), deg2rad(90)];



L1 = Link('d', 0.103,'a',0,'alpha',pi/2, 'qlim', q1Lim);
L2 = Link('d', 0,'a', 0.135,'alpha',0, 'qlim', q2Lim);
L3 = Link('d', 0,'a', 0.162,'alpha',0, 'qlim', q3Lim);
L4 = Link('d', 0,'a', 0.053,'alpha',pi/2);
L5 = Link('d', 0,'a', 0,'alpha',0, 'qlim', q5Lim);

doBot = SerialLink([L1 L2 L3 L4 L5], 'name', 'doBot');



%scale the plot
scale = 0.5;
workspace = [-0.5 0.5 -0.5 0.5 -0.05 1];
%Plot the two robots 
%q = [0 deg2rad(45) deg2rad(-90) 0 0];
q1 = 0;
q2 = deg2rad(45);
q3 = deg2rad(-90);
q4 = 2*pi - q3 - q2;
q5 = 0;
q = [q1 q2 q3 q4 q5];
%%



%%
for linkIndex = 0:5
    [ faceData, vertexData, plyData{linkIndex+1} ] = plyread(['J',num2str(linkIndex),'.ply'],'tri');
    doBot.faces{linkIndex+1} = faceData;
    doBot.points{linkIndex+1} = vertexData;
       
end

    % Display robot
    doBot.plot3d(q,'workspace',workspace);
    camlight;
    doBot.delay = 0;
  


%doBot.plot(q,'workspace',workspace,'scale',scale,'trail','r-');
hold
%%


steps = 50

tray1 = transl(0.25,0.15,0.05);
ingredient1 = transl(0.25,0.15,0.01);

tray3 = transl(0.15,0.25,0.05);
ingredient3 = transl(0.15,0.25,0.01);

tray2 = transl(0.20, 0.20, 0.05)
ingredient2 = transl(0.20, 0.20, 0.01)

assmPoint = transl(0.28, 0, 0.05);
burger = transl(0.28, 0, 0.01);
burger2 = transl(0.28, 0, 0.013);
burger3 = transl(0.28, 0, 0.016);
burger4 = transl(0.28, 0, 0.019);

%%
%Ingredient 1 modeling
[f,v,data] = plyread('ingredient1.ply','tri');

ingredient1VertexCount = size(v,1);

midPoint = sum(v)/ingredient1VertexCount;
ingredient1Verts = v - repmat(midPoint,ingredient1VertexCount,1);

ingredienet1Pose = ingredient1;

vertexColours = [data.vertex.red, data.vertex.green, data.vertex.blue] / 255;

ingredient1Mesh = trisurf(f,ingredient1Verts(:,1),ingredient1Verts(:,2), ingredient1Verts(:,3) ...
    ,'FaceVertexCData',vertexColours,'EdgeColor','interp','EdgeLighting','flat');

updatedPoints = [ingredienet1Pose * [ingredient1Verts,ones(ingredient1VertexCount,1)]']'; 
ingredient1Mesh.Vertices = updatedPoints(:,1:3);
%%
%ingredient 2 modeling
[f2,v2,data2] = plyread('ingredient2.ply','tri');

ingredient2VertexCount = size(v2,1);

midPoint2 = sum(v2)/ingredient2VertexCount;
ingredient2Verts = v2 - repmat(midPoint2,ingredient2VertexCount,1);

ingredient22Pose = ingredient2;

vertexColours2 = [data2.vertex.red, data2.vertex.green, data2.vertex.blue] / 255;

ingredient2Mesh = trisurf(f2,ingredient2Verts(:,1),ingredient2Verts(:,2), ingredient2Verts(:,3) ...
    ,'FaceVertexCData',vertexColours2,'EdgeColor','interp','EdgeLighting','flat');

updatedPoints2 = [ingredient22Pose * [ingredient2Verts,ones(ingredient2VertexCount,1)]']'; 
ingredient2Mesh.Vertices = updatedPoints2(:,1:3);
%%
%ingredient 3 modeling
[f3,v3,data3] = plyread('ingredient3.ply','tri');

ingredient3VertexCount = size(v3,1);

midPoint3 = sum(v3)/ingredient3VertexCount;
ingredient3Verts = v3 - repmat(midPoint3,ingredient3VertexCount,1);

ingredient3Pose = ingredient3;

vertexColours3 = [data3.vertex.red, data3.vertex.green, data3.vertex.blue] / 255;

ingredient3Mesh = trisurf(f3,ingredient3Verts(:,1),ingredient3Verts(:,2), ingredient3Verts(:,3) ...
    ,'FaceVertexCData',vertexColours3,'EdgeColor','interp','EdgeLighting','flat');

updatedPoints3 = [ingredient3Pose * [ingredient3Verts,ones(ingredient3VertexCount,1)]']'; 
ingredient3Mesh.Vertices = updatedPoints3(:,1:3);

%%
%Ingredient 4 modeling
[f,v,data] = plyread('ingredient1.ply','tri');

ingredient4VertexCount = size(v,1);

midPoint = sum(v)/ingredient4VertexCount;
ingredient4Verts = v - repmat(midPoint,ingredient4VertexCount,1);

ingredienet4Pose = ingredient1;

vertexColours = [data.vertex.red, data.vertex.green, data.vertex.blue] / 255;

ingredient4Mesh = trisurf(f,ingredient4Verts(:,1),ingredient4Verts(:,2), ingredient4Verts(:,3) ...
    ,'FaceVertexCData',vertexColours,'EdgeColor','interp','EdgeLighting','flat');

updatedPoints4 = [ingredienet4Pose * [ingredient4Verts,ones(ingredient4VertexCount,1)]']'; 
ingredient4Mesh.Vertices = updatedPoints4(:,1:3);

%% Environment modeling
%Creating Wall no.1
[f,v,data] = plyread('wall1.ply','tri');

wallpos = transl(0.4, 0.5, 0);
wall1VertexCount = size(v,1);

midPoint = sum(v)/wall1VertexCount;
wall1Verts = v - repmat(midPoint,wall1VertexCount,1);

wall1Pose = wallpos;

vertexColours = [data.vertex.red, data.vertex.green, data.vertex.blue] / 255;

wall1Mesh = trisurf(f,wall1Verts(:,1),wall1Verts(:,2), wall1Verts(:,3) ...
    ,'FaceVertexCData',vertexColours,'EdgeColor','interp','EdgeLighting','flat');

updatedPoints = [wall1Pose * [wall1Verts,ones(wall1VertexCount,1)]']'; 
wall1Mesh.Vertices = updatedPoints(:,1:3);

%% Creating Wall no.2

[f,v,data] = plyread('wall1.ply','tri');

wallpos2 = transl(0.4, -0.5, 0);
wall2VertexCount = size(v,1);

midPoint = sum(v)/wall2VertexCount;
wall2Verts = v - repmat(midPoint,wall2VertexCount,1);

wall2Pose = wallpos2;

vertexColours = [data.vertex.red, data.vertex.green, data.vertex.blue] / 255;

wall2Mesh = trisurf(f,wall2Verts(:,1),wall2Verts(:,2), wall2Verts(:,3) ...
    ,'FaceVertexCData',vertexColours,'EdgeColor','interp','EdgeLighting','flat');

updatedPoints2 = [wall2Pose * [wall2Verts,ones(wall2VertexCount,1)]']'; 
wall2Mesh.Vertices = updatedPoints2(:,1:3);

%% Creating Wall no.3
[f,v,data] = plyread('wall2.ply','tri');

wallpos3 = transl(-0.12, 0, 0);
wall3VertexCount = size(v,1);

midPoint = sum(v)/wall3VertexCount;
wall3Verts = v - repmat(midPoint,wall3VertexCount,1);

wall3Pose = wallpos3;

vertexColours = [data.vertex.red, data.vertex.green, data.vertex.blue] / 255;

wall3Mesh = trisurf(f,wall3Verts(:,1),wall3Verts(:,2), wall3Verts(:,3) ...
    ,'FaceVertexCData',vertexColours,'EdgeColor','interp','EdgeLighting','flat');

updatedPoints3 = [wall3Pose * [wall3Verts,ones(wall3VertexCount,1)]']'; 
wall3Mesh.Vertices = updatedPoints3(:,1:3);
%%
    function a = doBotMove(transform, ingredient)
        TR = transform;
        mask = [1,1,1,0,0,0];
        TR1 = doBot.getpos();
        qgoal = doBot.ikine(TR,TR1,mask);
        doBotJointMatrix = jtraj(TR1,qgoal,steps);
        for i=1:steps
            for j=1:3
                if doBotJointMatrix(i,j) < doBot.qlim(j,1);
                    doBotJointMatrix(i,j) = doBot.qlim(j,1);
                elseif doBotJointMatrix(i,j) > doBot.qlim(j,2);
                    doBotJointMatrix(i,j) = doBot.qlim(j,2);
                end
            end
            doBotJointMatrix(i,4) = (2*pi - doBotJointMatrix(i,3) - doBotJointMatrix(i,2));
            
        end
        if ingredient == 0
            doBot.animate(doBotJointMatrix);
        else
            for i=1:steps
                doBot.animate(doBotJointMatrix(i,:));
                eTR = doBot.fkine(doBotJointMatrix(i,:));
                if ingredient == 1
                    updatedPoints = [eTR * [ingredient1Verts,ones(ingredient1VertexCount,1)]']';
                    ingredient1Mesh.Vertices = updatedPoints(:,1:3);
                elseif ingredient == 2
                    updatedPoints2 = [eTR * [ingredient2Verts,ones(ingredient2VertexCount,1)]']';
                    ingredient2Mesh.Vertices = updatedPoints2(:,1:3);
                elseif ingredient == 3
                    updatedPoints3 = [eTR * [ingredient3Verts,ones(ingredient3VertexCount,1)]']';
                    ingredient3Mesh.Vertices = updatedPoints3(:,1:3);
                elseif ingredient == 5
                    updatedPoints4 = [eTR * [ingredient4Verts,ones(ingredient4VertexCount,1)]']';
                    ingredient4Mesh.Vertices = updatedPoints4(:,1:3);    
                end
                if ~mod(steps,5)
                    drawnow();
                end
            end
        end
        a = 1;
        hold
        
    end 
%%
TR = tray1;
a = doBotMove(TR, 4)

TR = ingredient1;
a = doBotMove(TR, 4)

TR = tray1;
a = doBotMove(TR, 1)

TR = assmPoint
a = doBotMove(TR, 1)

TR = burger;
a = doBotMove(TR, 1)

TR = tray2;
a = doBotMove(TR, 4)

TR = ingredient2;
a = doBotMove(TR, 4)

TR = tray2;
a = doBotMove(TR, 2)

TR = assmPoint;
a = doBotMove(TR, 2)

TR = burger2;
a = doBotMove(TR, 2)

TR = tray3;
a = doBotMove(TR, 4)

TR = ingredient3;
a = doBotMove(TR, 4)

TR = tray3;
a = doBotMove(TR, 3)


TR = assmPoint;
a = doBotMove(TR, 3)

TR = burger3; 
a = doBotMove(TR, 3)

TR = tray1;
a = doBotMove(TR, 4)

TR = ingredient1;
a = doBotMove(TR, 4)

TR = tray1;
a = doBotMove(TR, 5)

TR = assmPoint
a = doBotMove(TR, 5)

TR = burger4;
a = doBotMove(TR, 5)


%%

end