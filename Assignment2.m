function ass2_DoBot
%% Clear and make docked figures
close all
clc
clear all
set(0, 'DefaultFigureWindowStyle','docked')
%% Define the robot
base = transl(0, 0, 0.2);
q1Lim = [deg2rad(-135), deg2rad(135)];
q2Lim = [deg2rad(-5), deg2rad(85)];
q3Lim = [deg2rad(-95), deg2rad(10)];
q5Lim = [deg2rad(-90), deg2rad(90)];

L1 = Link('d', 0.103,'a',0,'alpha',pi/2, 'qlim', q1Lim);
L2 = Link('d', 0,'a', 0.135,'alpha',0, 'qlim', q2Lim);
L3 = Link('d', 0,'a', 0.162,'alpha',0, 'qlim', q3Lim);
L4 = Link('d', 0,'a', 0.053,'alpha',pi/2);
L5 = Link('d', 0,'a', 0,'alpha',0, 'qlim', q5Lim);

doBot = SerialLink([L1 L2 L3 L4 L5], 'name', 'doBot','base',base);
% doBot.plotopt = {'nojoints'};
%% Plot the robot

%scale the plot
scale = 0.4; 
workspace = [-0.5 0.5 -0.5 0.5 -0.05 1];
%Plot the robot
q = [0 deg2rad(45) deg2rad(0) pi/4 0];
%doBot.tool = troty(-pi/2);
doBot.plot(q,'workspace',workspace,'scale',scale,'trail','r-');

hold
%hold 
dq = doBot.getpos();
ddq2 = dq(2);
ddq3 = dq(3);
ddq4 = 2*pi - ddq3 - ddq2;
q = [0 deg2rad(45) deg2rad(0) ddq4 0];
q0 = q;
doBot.plot(q,'workspace',workspace,'scale',scale,'trail','r-');
hold
doBot.teach()
%% Define trays and assembly point
%defining the tray locations
tray1 = transl(-0.25, -0.25, 0.3);
tray2 = transl(0, -0.4, 0.3);
tray3 = transl(0.1, -0.32, 0.3);

%define ammount of ingredients in each tray to be picked
quantity = [2 3 2];

%% start cycle for tray 1
%chose tray to pick ingredients from
chosenPart = tray1;
M = [1 1 1 0 0 0];
doBotStart = doBot.getpos();
doBotEnd = chosenPart;
pick1 = doBot.ikine(doBotEnd,doBotStart,M)
for pickSteps = 1:quantity(1)
    jointTrajectory = jtraj(doBotStart,pick1,20);
    for trajStep = 1:size(jointTrajectory,1)
            dq = doBot.getpos();
            ddq2 = dq(2);
            ddq3 = dq(3);
            ddq4 = 2*pi - ddq3 - ddq2;
            q = jointTrajectory(trajStep,:);
            q(1,4) = ddq4;
            doBot.animate(q);
            drawnow();
            pause(0);
    end
    %return to assembly point
    drop = q0;
    jointTrajectory = jtraj(doBot.getpos(),drop,20);
    for trajStep = 1:size(jointTrajectory,1)
            dq = doBot.getpos();
            ddq2 = dq(2);
            ddq3 = dq(3);
            ddq4 = 2*pi - ddq3 - ddq2;
            q = jointTrajectory(trajStep,:);
            q(1,4) = ddq4;
            doBot.animate(q);
            drawnow();
            pause(0);
    end
end
%% Start cycle for tray 2
chosenPart = tray2;
doBotStart = doBot.getpos();
doBotEnd = chosenPart;
pick2 = doBot.ikine(doBotEnd,doBotStart,M);
for pickSteps = 1:quantity(2)
    %chose tray to pick ingredients from
    jointTrajectory = jtraj(doBot.getpos(),pick2,20);
    for trajStep = 1:size(jointTrajectory,1)
            dq = doBot.getpos();
            ddq2 = dq(2);
            ddq3 = dq(3);
            ddq4 = 2*pi - ddq3 - ddq2;
            q = jointTrajectory(trajStep,:);
            q(1,4) = ddq4;
            doBot.animate(q);
            drawnow();
            pause(0);
    end
    %return to assembly point
    drop = q0;
    jointTrajectory = jtraj(doBot.getpos(),drop,20);
    for trajStep = 1:size(jointTrajectory,1)
            dq = doBot.getpos();
            ddq2 = dq(2);
            ddq3 = dq(3);
            ddq4 = 2*pi - ddq3 - ddq2;
            q = jointTrajectory(trajStep,:);
            q(1,4) = ddq4;
            doBot.animate(q);
            drawnow();
            pause(0);
    end
end
%% start cycle for tray 3
%chose tray to pick ingredients from
chosenPart = tray3;
doBotStart = doBot.getpos();
doBotEnd = chosenPart;
pick3 = doBot.ikine(doBotEnd,doBotStart,M);
for pickSteps = 1:quantity(3)
    jointTrajectory = jtraj(doBot.getpos(),pick3,20);
    for trajStep = 1:size(jointTrajectory,1)
            dq = doBot.getpos();
            ddq2 = dq(2);
            ddq3 = dq(3);
            ddq4 = 2*pi - ddq3 - ddq2;
            q = jointTrajectory(trajStep,:);
            q(1,4) = ddq4;
            doBot.animate(q);
            drawnow();
            pause(0);
    end
    %return to assembly point
    drop = q0;
    jointTrajectory = jtraj(doBot.getpos(),drop,20);
    for trajStep = 1:size(jointTrajectory,1)
            dq = doBot.getpos();
            ddq2 = dq(2);
            ddq3 = dq(3);
            ddq4 = 2*pi - ddq3 - ddq2;
            q = jointTrajectory(trajStep,:);
            q(1,4) = ddq4;
            doBot.animate(q);
            drawnow();
            pause(0);
    end
    
end

% %% working space
% model = 2;
% 
% stepRads = deg2rad(15);
% qlim = doBot.qlim;
% 
% pointCloudeSize = prod(floor((qlim(1:5,2)-qlim(1:5,1))/stepRads + 1));
% pointCloud = zeros(pointCloudeSize,3);
% counter = 1;
% maximumReach = 0;
% for q1 = qlim(1,1):stepRads:qlim(1,2)
%     for q2 = qlim(2,1):stepRads:qlim(2,2)
%         for q3 = qlim(3,1):stepRads:qlim(3,2)
%             for q4 = qlim(4,1):stepRads:qlim(4,2)
%                 for q5 = qlim(5,1):stepRads:qlim(5,2)
%                             q = [q1,q2,q3,q4,q5];
%                             tr = doBot.fkine(q);                        
%                             pointCloud(counter,:) = tr(1:3,4)';
%                             if (pointCloud(counter, 1) > maximumReach)
%                                 maximumReach = pointCloud(counter, 1);
%                             end                          
%                             counter = counter + 1;                    
%                 end
%             end
%         end
%     end
% end
% 
% % 2.6 Create a 3D model showing where the end effector can be over all these samples.  
% plot3(pointCloud(:,1),pointCloud(:,2),pointCloud(:,3),'r.');
% axis equal

%% Loading models into Matlab
% Turn on a light (only turn on 1, don't keep turning them on), and make axis equal
camlight;
axis equal;
view(3);
hold on;
keyboard

%% Load 1st Arm into Matlab
[f,v,data] = plyread('arm1.ply','tri');

% Scale the colours to be 0-to-1 (they are originally 0-to-255
vertexColours = [data.vertex.red, data.vertex.green, data.vertex.blue] / 255;

% Then plot the trisurf
arm1_h = trisurf(f,v(:,1),v(:,2), v(:,3) ...
    ,'FaceVertexCData',vertexColours,'EdgeColor','interp','EdgeLighting','flat');
keyboard

%% Load 2nd Arm into Matlab
[f,v,data] = plyread('arm2.ply','tri');

% Scale the colours to be 0-to-1 (they are originally 0-to-255
vertexColours = [data.vertex.red, data.vertex.green, data.vertex.blue] / 255;

% Then plot the trisurf
arm2_h = trisurf(f,v(:,1),v(:,2), v(:,3) ...
    ,'FaceVertexCData',vertexColours,'EdgeColor','interp','EdgeLighting','flat');
keyboard

%% Load Base into Matlab
[f,v,data] = plyread('base.ply','tri');

% Scale the colours to be 0-to-1 (they are originally 0-to-255
vertexColours = [data.vertex.red, data.vertex.green, data.vertex.blue] / 255;

% Then plot the trisurf
base_h = trisurf(f,v(:,1),v(:,2), v(:,3) ...
    ,'FaceVertexCData',vertexColours,'EdgeColor','interp','EdgeLighting','flat');

keyboard

%% Plot and Colour DoBot
%doBot = PlotAndColourRobot(doBot,workspace)
    for linkIndex = 0:doBot.n
        %[ faceData, vertexData, plyData{linkIndex+1} ] = plyread(['J',num2str(linkIndex),'.ply'],'tri');
        %doBot.faces{linkIndex+1} = faceData;
        %doBot.points{linkIndex+1} = vertexData;
    end

    % Display robot
    %doBot.plot3d(zeros(1,doBot.n),'noarrow','workspace',workspace);
    %camlight
    %doBot.delay = 0;

    % Try to correctly colour the arm (if colours are in ply file data)
	handles = findobj('Tag', doBot.name);
	h = get(handles,'UserData');
    for linkIndex = 0:5
        try 
            doBot.faces{linkIndex+1} = faceData;
            doBot.points{linkIndex+1} = vertexData;
        catch ME_1
            disp(ME_1);
            continue;
        end
    end  



