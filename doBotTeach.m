function [] = doBotTeach(q, init)

if init == 1
    
    q1Lim = [deg2rad(-135), deg2rad(135)];
    q2Lim = [deg2rad(5), deg2rad(80)];
    q3Lim = [deg2rad(-170), deg2rad(5)];
    q5Lim = [deg2rad(-90), deg2rad(90)];



    L1 = Link('d', 0.103,'a',0,'alpha',pi/2, 'qlim', q1Lim);
    L2 = Link('d', 0,'a', 0.135,'alpha',0, 'qlim', q2Lim);
    L3 = Link('d', 0,'a', 0.162,'alpha',0, 'qlim', q3Lim);
    L4 = Link('d', 0,'a', 0.053,'alpha',pi/2);
    L5 = Link('d', 0,'a', 0,'alpha',0, 'qlim', q5Lim);

    doBot = SerialLink([L1 L2 L3 L4 L5], 'name', 'doBot');



%scale the plot
    scale = 0.5;
    workspace = [-0.5 0.5 -0.5 0.5 -0.05 1];

% q4 = 2*pi - q3 - q2;
% 

    doBot.plot(q,'workspace',workspace,'scale',scale,'trail','r-');
else
    scale = 0.5;
    workspace = [-0.5 0.5 -0.5 0.5 -0.05 1];
    doBot.plot(q,'workspace',workspace,'scale',scale,'trail','r-');
end

end